// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// USER REGISTRATION ROUTE
router.post('/userRegistration', userControllers.registerUser);

// USER LOGIN ROUTE
router.post('/login', userControllers.loginUser);

// UPDATE TO ADMIN ROUTE
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

// USER CHECKOUT ROUTE
router.post('/userCheckout', verify, userControllers.userCheckout);

module.exports = router;