// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const orderControllers = require('../controllers/orderControllers');
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// ORDER CREATION ROUTE
// router.post('/createOrder', verify, orderControllers.createOrder);

// RETRIEVE USER ORDERS ROUTE
router.get('/userOrders', verify, orderControllers.userOrders);

// RETRIEVE ALL ORDERS ROUTE
router.get('/allOrders', verify, verifyAdmin, orderControllers.getAllOrders);

// CREATE ORDER CREATION ALTERNATIVE ROUTE
router.post("/orderCheckout", verify, orderControllers.orderCheckout);

// ADD PRODUCT TO EXISTING ORDER ROUTE
// router.put("/updateOrder/:id", verify, orderControllers.updateOrder);

router.put("/addToOrder/:id", verify, orderControllers.addToOrder);

module.exports = router;