// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const productControllers = require('../controllers/productControllers');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// RETRIEVE ALL PRODUCTS ROUTE
router.get('/getAllActiveProducts', productControllers.getAllActiveProducts);

// RETRIEVE SINGLE PRODUCT ROUTE
router.get('/getSingleProduct/:id', productControllers.getSingleProduct);

// PRODUCT CREATION ROUTE
router.post('/createProduct', verify, verifyAdmin, productControllers.addProduct);

// UPDATE PRODUCT ROUTE
router.put('/updateProduct/:id', verify, verifyAdmin, productControllers.updateProduct);

// ARCHIVE PRODUCT
router.put('/archiveProduct/:id', verify, verifyAdmin, productControllers.archiveProduct);

module.exports = router;