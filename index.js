// DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// SERVER
const app = express();
const port = process.env.PORT || 8000;

// DATABASE CONNECTION
mongoose.connect("mongodb+srv://JPGabutin:admin@cluster0.7yfzd.mongodb.net/capstone-2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// GROUP ROUTING
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);
const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders', orderRoutes);

// PORT LISTENER
app.listen(port, () => console.log(`Server is running at port ${port}`));