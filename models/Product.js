// DEPENDENCIES
const mongoose = require('mongoose');
// PRODUCT SCHEMA
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, 'Product name required']
	},
	description: {
		type: String,
		require: [true, 'Product description required']
	},
	price: {
		type: Number,
		require: [true, 'Product price required']
	},
	inStock: {
		type: Boolean,
		default: true
	},
	dateFirstAvailable: {
		type: Date,
		default: new Date()
	},
	consumers: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	]
});
module.exports = mongoose.model("Product", productSchema);