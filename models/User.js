// DEPENDENCIES
const mongoose = require('mongoose');
// USER SCHEMA
const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username required"]
	},
	firstName: {
		type: String,
		required: [true, "First name required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name required"]
	},
	email: {
		type: String,
		required: [true, "Email required"]
	},
	password: {
		type: String,
		required: [true, "Password required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID required"]
			},
			datePurchased: {
				type: Date,
				default: new Date()
			}
		}
	]
});
module.exports = mongoose.model("User", userSchema);