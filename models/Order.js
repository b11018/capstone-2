// DEPENDENCIES
const mongoose = require('mongoose');
// ORDER SCHEMA
const orderSchema = new mongoose.Schema([
	{
		purchaser: {
			type: String,
			required: [true, "Purchaser is required"]
		},
		productName: {
			type: String,
			required: [true, "Product Name is required"]
		},
		productQuantity: {
			type: Number,
			required: [true, "Quantity is required"]
		},
		totalAmount: {
			type: Number,
			required: [true, "Total Amount is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		orderDetails: [
			{
				userId: {
					type: String,
					required: [true, "User Id is required"]
				},
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				}
			}
		]
	}
]);

module.exports = mongoose.model("Order", orderSchema);