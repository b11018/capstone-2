// DEPENDENCIES
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// USER REGISTRATION
module.exports.registerUser = (req, res) => {
	console.log(req.body);
	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if(foundEmail !== null){
			return res.send("Email is already registered!")
		} else {
			const hashedPW = bcrypt.hashSync(req.body.password, 10);
			let newUser = new User({
				username: req.body.username,
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				mobileNo: req.body.mobileNo,
				email: req.body.email,
				password: hashedPW
			});
			newUser.save()
			.then(user => res.send(user))
			.catch(err => res.send(err));
		}
	})
	.catch(err => res.send(err));
	
};

// USER LOGIN
module.exports.loginUser = (req, res) => {
	console.log(req.body);
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("No user found in the database");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			console.log(isPasswordCorrect);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send("Incorrect password, please try again");
			}
		} 
	})
	.catch(err => res.send(err));
};

// UPDATE TO ADMIN
module.exports.updateAdmin = (req, res) => {
	let updates = {
		isAdmin: true
	};
	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// USER CHECKOUT (NON-ADMIN)
module.exports.userCheckout = async (req, res) => {
	// Checks if Admin
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	};
	// Searches for user and updates its 'orders' subdoc
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newPurchase = {
			productId: req.body.productId
		};
		user.orders.push(newPurchase);
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	};
	// Searches for product and updates its 'consumers' subdoc
	let isProductUpdated = await Product.findById(req.body.productId).then(product => {
		let consumer = {
			userId: req.user.id,
			quantity: req.body.quantity
		};
		product.consumers.push(consumer)
		return product.save()
		.then(product => true)
		.catch(err => err.message);
	});
	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated})
	};
	// CREATE A THIRD FOR ORDER UPDATES SIMILAR TO TWO ABOVE
	let isOrderUpdated = await Order.findById(req.body.orderId).then(order => {
		let newOrder = {
			userId: req.user.id,
			productId: req.body.productId
		};
		order.orderDetails.push(newOrder)
		return order.save()
		.then(order => true)
		.catch(err => err.message);
	});
	if(isOrderUpdated !== true){
		return res.send({message: isOrderUpdated})
	};
	// Checks if all models are updated
	if(isUserUpdated && isProductUpdated && isOrderUpdated){
		return res.send({message: 'User Purchase successful'})
	};
};