// IMPORT PRODUCT MODEL
const Product = require('../models/Product');

// RETRIEVE ALL PRODUCTS
module.exports.getAllActiveProducts = (req, res) => {
	Product.find({isActive: true})
	.then(activeProducts => res.send(activeProducts))
	.catch(err => res.send(err));
};

// RETRIEVE SINGLE PRODUCT
module.exports.getSingleProduct = (req, res) => {
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// PRODUCT CREATION (ADMIN)
module.exports.addProduct = (req, res) => {
	console.log(req.body);
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});
	newProduct.save()
	.then(product => res.send(product))
	.catch(err => res.send(err));
};

// UPDATE PRODUCT
module.exports.updateProduct = (req, res) => {
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));
};

// ARCHIVE PRODUCT
module.exports.archiveProduct = (req, res) => {
	let updates = {
		inStock: false
	};
	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(archivedProduct => res.send(archivedProduct))
	.catch(err => res.send(err));
};